#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <stdbool.h>

void buatFolder(const char *namaFolder) {
    struct stat st = {0};
    if (stat(namaFolder, &st) == -1) {
        mkdir(namaFolder, 0777);
    }
}

void unduhBerkas() {
    buatFolder("sisop_terinfeksi");

    chdir("sisop_terinfeksi");

    char *const argv[] = {"curl", "-L", "-o", "ekstensi.csv", "https://drive.google.com/uc?id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5", NULL};

    execvp("curl", argv);
    perror("execvp");
    exit(1);
}

void simpanBerkas() {
    if (access("sisop_terinfeksi/ekstensi.csv", F_OK) == -1) {
        int status;
        pid_t pid = fork();

        if (pid == 0) {
            unduhBerkas();
            exit(0);
        } else if (pid < 0) {
            perror("Terjadi kesalahan selama fork.");
            exit(1);
        } else {
            waitpid(pid, &status, 0);
            if (WEXITSTATUS(status) != 0) {
                fprintf(stderr, "Gagal mengunduh berkas.\n ");
                exit(1);
            }
        }
    }
}

void periksaBerkas() {
    FILE *daftarVirus = fopen("sisop_terinfeksi/terdekripsi.csv", "r");

    if (daftarVirus == NULL) {
        perror("Kesalahan saat membuka terdekripsi.csv");
        exit(1);
    }

    char baris[256];
    bool virusTerdeteksi = false;

    while (fgets(baris, sizeof(baris), daftarVirus)) {
        virusTerdeteksi = false;

        baris[strcspn(baris, "\n")] = 0;

        DIR *direktori;
        struct dirent *entri;

        direktori = opendir("sisop_terinfeksi");

        if (direktori == NULL) {
            perror("Kesalahan membuka direktori");
            exit(1);
        }

        while ((entri = readdir(direktori)) != NULL) {
            if (entri->d_type == DT_REG) {
                char *ekstensiBerkas = strrchr(entri->d_name, '.');
                if (ekstensiBerkas != NULL) {
                    ekstensiBerkas++;
                    if (strstr(baris, ekstensiBerkas) != NULL) {
                        buatLog(entri->d_name, "Dipindahkan ke karantina");
                        virusTerdeteksi = true;
                    }
                }
            }
        }

        closedir(direktori);

        if (!virusTerdeteksi) {
            printf("Tidak ada virus terdeteksi.\n");
        }
    }

    fclose(daftarVirus);
}

void buatLog(const char *baris, const char *aksi) {
    time_t sekarang;
    struct tm *waktuSekarang;

    time(&sekarang);
    waktuSekarang = localtime(&sekarang);

    char waktuLog[25];
    strftime(waktuLog, sizeof(waktuLog), "%d-%m-%y:%H-%M-%S", waktuSekarang);

    FILE *berkasLog = fopen("virus.log", "a");
    if (berkasLog == NULL) {
        perror("Kesalahan saat membuka berkas log.");
        exit(1);
    }

    char *namaPengguna = getenv("USER");
    fprintf(berkasLog, "[%s][%s] - %s - %s\n", namaPengguna, waktuLog, baris, aksi);

    fclose(berkasLog);
}

void dekripsiBerkas() {
    FILE *belumTerdekripsi = fopen("sisop_terinfeksi/ekstensi.csv", "r");
    FILE *terdekripsi = fopen("sisop_terinfeksi/terdekripsi.csv", "w");

    if (belumTerdekripsi == NULL || terdekripsi == NULL) {
        perror("Kesalahan saat membuka berkas.");
        exit(1);
    }

    char baris[4096];
    int nomorBaris = 0;

    while (fgets(baris, sizeof(baris), belumTerdekripsi)) {
        nomorBaris++;

        if (nomorBaris <= 8) {
            continue;
        }

        for (int i = 0; i < strlen(baris); i++) {
            if ('A' <= baris[i] && baris[i] <= 'Z') {
                baris[i] = ((baris[i] - 'A' + 13) % 26) + 'a';
            } else if ('a' <= baris[i] && baris[i] <= 'z') {
                baris[i] = ((baris[i] - 'a' + 13) % 26) + 'a';
            }
        }

        fprintf(terdekripsi, "%s", baris);
    }

    fclose(belumTerdekripsi);
    fclose(terdekripsi);
}

int main(int argc, char const *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Penggunaan: %s <mode>\n", argv[0]);
        exit(1);
    }

    char mode = argv[1][0];

    switch (mode) {
        case 'u':
            simpanBerkas();
            dekripsiBerkas();
            break;
        case 'p':
            periksaBerkas();
            break;
        default:
            fprintf(stderr, "Mode tidak valid. Gunakan 'u' untuk mode unduh atau 'p' untuk mode periksa.\n");
            exit(1);
    }

    return 0;
}
