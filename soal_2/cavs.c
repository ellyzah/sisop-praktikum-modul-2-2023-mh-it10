#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

// deklarasi global untuk arahan soal
int pg = 0, sg = 0, sf = 0, pf = 0, c = 0;

void buatFolder(char *folderName) {
    pid_t child = fork();
    if (child == 0) {
        // buat folder dulu
        char *argv[] = {"mkdir", "-p", folderName, NULL};
        execv("/bin/mkdir", argv);
    }
    wait(NULL);
}


// berikan fungsi untuk unduh data pemain
void unduhDatabasePemain() {
    // seperti biasa pakai fork untuk jalankan child proses
    pid_t child = fork();
    if (child == 0) {
        // lakukan get download untuk dapetin data pemainnya
        char *argv[] = {"wget", "--no-check-certificate", "https://drive.google.com/u/0/uc?id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK&export=download", "-O", "pemain.zip", NULL};
        execv("/usr/bin/wget", argv);
    }
    wait(NULL);
}

// jangan lupa buat ekstrak yak
void ekstrakDatabase() {
    // seperti biasa pakai fork untuk jalankan child proses
    pid_t child = fork();
    if (child == 0) {
        // perunzipan duniawi
        char *argv[] = {"unzip", "pemain.zip", "-d", "players", NULL};
        execv("/usr/bin/unzip", argv);
    }
    wait(NULL);
}


// kondisi ketika pemain.zip udah dapet semua gambar-gambarnya, jadi perlu untuk dihapus
void hapusDatabaseZip() {
    // seperti biasa pakai fork untuk jalankan child proses
    pid_t child = fork();
    if (child == 0) {
        // hapus pemain.zip yang udah ga kepakai biar ga menuhin storage juga
        char *argv[] = {"rm", "pemain.zip", NULL};
        execv("/bin/rm", argv);
    }
    wait(NULL);
}

// nah ini juga hapus pemain selain cavaliers, karna sebelumnya itu banyak filename png selain cavaliers
void hapusPemainSelainCavs() {
    pid_t child = fork();
    if (child == 0) {
        // proses menemukan cavaliers
        char *argv[] = {"find", "players", "-type", "f", "!", "-name", "*Cavaliers*.png", "-delete", NULL};
        execv("/usr/bin/find", argv);
    }
    wait(NULL);
}


// buat bedain kategori-kategori pemain yak
void kategorikanPemain(char *namaFile, int *pg, int *sg, int *sf, int *pf, int *c) {

    // langsung aja pakai kondisi, jika pemain merupakan pg / sg dst buatkan folder sendiri"
    if (strstr(namaFile, "PG") != NULL) {
        buatFolder("players/PG");
        pid_t child = fork();
        if (child == 0) {
            char command[100];
            // buat move pemain ke folder sesuai kategori
            sprintf(command, "mv players/%s players/PG/%s", namaFile, namaFile);
            char *argv[] = {"sh", "-c", command, NULL};
            execv("/bin/sh", argv);
        }
        wait(NULL);
        (*pg)++;
    } else if (strstr(namaFile, "SG") != NULL) {
        buatFolder("players/SG");
        pid_t child = fork();
        if (child == 0) {
            char command[100];
            sprintf(command, "mv players/%s players/SG/%s", namaFile, namaFile);
            char *argv[] = {"sh", "-c", command, NULL};
            execv("/bin/sh", argv);
        }
        wait(NULL);
        (*sg)++;
    } else if (strstr(namaFile, "SF") != NULL) {
        buatFolder("players/SF");
        pid_t child = fork();
        if (child == 0) {
            char command[100];
            sprintf(command, "mv players/%s players/SF/%s", namaFile, namaFile);
            char *argv[] = {"sh", "-c", command, NULL};
            execv("/bin/sh", argv);
        }
        wait(NULL);
        (*sf)++;
    } else if (strstr(namaFile, "PF") != NULL) {
        buatFolder("players/PF");
        pid_t child = fork();
        if (child == 0) {
            char command[100];
            sprintf(command, "mv players/%s players/PF/%s", namaFile, namaFile);
            char *argv[] = {"sh", "-c", command, NULL};
            execv("/bin/sh", argv);
        }
        wait(NULL);
        (*pf)++;
    } else if (strstr(namaFile, "C") != NULL) {
        buatFolder("players/C");
        pid_t child = fork();
        if (child == 0) {
            char command[100];
            sprintf(command, "mv players/%s players/C/%s", namaFile, namaFile);
            char *argv[] = {"sh", "-c", command, NULL};
            execv("/bin/sh", argv);
        }
        wait(NULL);
        (*c)++;
    }
}


// buat nambahin orang yang menang 7 game winning
void tambahFotoClutch() {
    pid_t child = fork();
    if (child == 0) {
        char *argv[] = {"find", "players/PG", "-name", "*Cavaliers-PG-Kyrie-Irving.png", "-exec", "mv", "{}", "clutch/", ";", NULL};
        execv("/usr/bin/find", argv);
    }
    wait(NULL);
}


// buat nambain pemain yang dapat gelar theblock
void tambahFotoTheBlock() {
    pid_t child = fork();
    if (child == 0) {
        char *argv[] = {"find", "players/SF", "-name", "*Cavaliers-SF-LeBron-James.png", "-exec", "mv", "{}", "clutch/", ";", NULL};
        execv("/usr/bin/find", argv);
    }
    wait(NULL);
}


// nah ini buat formasi kategori yang udah dideklarasiin secara global
void buatFileFormasi() {
    FILE *fp = fopen("Formasi.txt", "w");
    if (fp == NULL) {
        printf("Error creating file!\n");
        return;
    }
    fprintf(fp, "PG: %d\nSG: %d\nSF: %d\nPF: %d\nC: %d\n", pg, sg, sf, pf, c);
    fclose(fp);
}

int main() {
    // di int main ini diurutin apa-apa aja yang menjadi awal bermulanya program
    buatFolder("players");
    unduhDatabasePemain();
    ekstrakDatabase();
    hapusDatabaseZip();
    hapusPemainSelainCavs();

    FILE *fileList = popen("ls players", "r");
    char namaFile[100];

    while (fgets(namaFile, sizeof(namaFile), fileList)) {
        namaFile[strlen(namaFile)-1] = '\0'; 
        kategorikanPemain(namaFile, &pg, &sg, &sf, &pf, &c);
    }

    pclose(fileList);

    buatFolder("clutch");
    tambahFotoClutch();
    tambahFotoTheBlock();

    buatFileFormasi();

    return 0;
}
