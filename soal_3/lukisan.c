#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>
#include <signal.h>

void buatFolder(char* timestamp) {
    pid_t mkdir_child_id = fork();

    if (mkdir_child_id < 0) 
        exit(EXIT_FAILURE);

    if (mkdir_child_id == 0) {
        execlp("/bin/mkdir", "mkdir", timestamp, NULL);
        exit(0);
    }
}


// fungsi buat download gambarnya dari unsplash sesuai dengan arahan soal
void downloadImages(char* timestamp) {
    int i;
    // lakukan looping sampe 15 foto ya gess ya
    for (i = 0; i < 15; i++) {
        // pakai fork seperti biasa
        pid_t download_id = fork();

        if (download_id < 0){
            exit(EXIT_FAILURE);
        }

        if (download_id == 0) {
            // ketika kondisi download_id == 0 maka lakuin hal ini
            time_t sekarang = time(NULL);
            char link[60];
            // buat dapetin source photonya
            sprintf(link, "https://source.unsplash.com/%ldx%ld", (sekarang%1000)+50, (sekarang%1000)+50);

            time_t waktu_photo;
            struct tm *waktu_gambar_sekarang;
            char files[30];
            char down_dir[150];

            time(&waktu_photo);
            waktu_gambar_sekarang = localtime(&waktu_photo);

            strftime(files, sizeof(files), "%Y-%m-%d_%H:%M:%S", waktu_gambar_sekarang);
            sprintf(down_dir, "%s/%s.jpg", timestamp, files);

            char *arg[] = {"wget", "-q", "-O", down_dir, link, NULL};
            execv("/usr/bin/wget", arg);              
        }
        sleep(5); 
    }
}

// buat zip dengan kondidi parameter pointer timestamp
void zipFolder(char* timestamp) {
    int status;
    wait(&status);
    char nama_zip[100];
    sprintf(nama_zip, "%s.zip", timestamp);

    // seperti biasa pakei fork()
    pid_t id_zip = fork();

    if (id_zip < 0){
        exit(EXIT_FAILURE);
    }

    // jika id_zip == 0 maka remove nama_zip
    if (id_zip == 0){
        char *arg[] = {"zip", "-rm", nama_zip, timestamp, NULL};
        execv("/usr/bin/zip", arg);
    }
}

void killScript(char* mode) {
    FILE* kill_script = fopen("kill.sh", "w");
    fprintf(kill_script, "#!/bin/bash\n");

    if (strcmp("-a", mode) == 0) {
        fprintf(kill_script, "killall -9 lukisan\nrm $0\n");
    } else if (strcmp("-b", mode) == 0) {
        fprintf(kill_script, "kill_parent(){\n");
        fprintf(kill_script, "kill ${@: -1}\n");
        fprintf(kill_script, "}\n");
        fprintf(kill_script, "kill_parent $(pidof lukisan)\nrm $0\n");
    }

    fclose(kill_script);

    pid_t chmod_id = fork();
    if (chmod_id < 0){
        exit(EXIT_FAILURE);
    }
    if (chmod_id == 0){
        execlp("/bin/chmod", "chmod", "+x", "kill.sh", NULL);
        exit(0);
    }
}

int main(int argc, char** argv) {
    if (argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0)) {
        printf("Usage: %s -a|-b\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    pid_t cid = fork();
    if (cid < 0){
        exit(EXIT_FAILURE);
    }
    if (cid > 0){
        exit(EXIT_SUCCESS);
    }

    umask(0);

    pid_t sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    char mode[3];
    strcpy(mode, argv[1]);

    while (1) {
        time_t sekarang = time(NULL);
        time(&sekarang);
        struct tm* time_info;
        time_info = localtime(&sekarang);
        char timestamp[60];
        strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);

        buatFolder(timestamp);
        downloadImages(timestamp);
        zipFolder(timestamp);

        sleep(30);
    }

    killScript(mode);
}
