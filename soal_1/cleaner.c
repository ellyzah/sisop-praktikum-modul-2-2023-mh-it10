// initialisasi library-library yand diperlukan

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>

// deklarasi dulu buat banyaknya character
#define MAX_PATH 260
#define MAX_LINE 256

void write_log(const char *filePath) {
    // pertama membuat proses child dengan fork()
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    // menggunakan pengkondisian pada proses child
    if (pid == 0) {
        time_t waktu;
        struct tm *detail_waktu;
        char waktu_string[20];
        
        time(&waktu);
        detail_waktu = localtime(&waktu);
        strftime(waktu_string, sizeof(waktu_string), "%Y-%m-%d %H:%M:%S", detail_waktu);

        char LogEntry[200];
        snprintf(LogEntry, sizeof(LogEntry), "[%s] '%s' has been removed.\n", waktu_string, filePath);

        FILE *LogFile = fopen("/home/bhiss/BHISMA_KULIAH/SISOP/sisop-praktikum-modul-2-2023-mh-it10/soal_1/cleaner.log", "a");
        if (LogFile != NULL) {
            fputs(LogEntry, LogFile);
            fclose(LogFile);
        }

        // merupakan syntax untuk keluar dari proses child
        exit(EXIT_SUCCESS); 
    } else { 
        // jika pid != 0 maka dilakukan parent proses
        // deklarasi variable
        int status;
        // menunggu proses child selesai
        waitpid(pid, &status, 0); 
    }
}

// membuat fungsi sesuai arahan dari soal (cleaner)
void cleaner(const char *dir_path) {
    while (1) {
        // Buka direktori yang dituju dengan pointer *dir
        DIR *dir = opendir(dir_path);

        if (!dir) {
            perror("opendir");
            return;
        }

        struct dirent *entry;

        while ((entry = readdir(dir)) != NULL) {
            // Check apakah entri adalah direktori
            if (entry->d_type == DT_DIR) {
                // Jangan memproses entri "." dan ".."
                if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                    // Gabungkan dir path dengan nama direktori
                    char sub_dir_path[MAX_PATH];
                    snprintf(sub_dir_path, sizeof(sub_dir_path), "%s/%s", dir_path, entry->d_name);

                    // Proses direktori sub dengan memanggil cleaner rekursif
                    cleaner(sub_dir_path);
                }
            } else if (entry->d_type == DT_REG) {
                char filePath[MAX_PATH];
                snprintf(filePath, sizeof(filePath), "%s/%s", dir_path, entry->d_name);

                FILE *file = fopen(filePath, "r");

                if (file) {
                    char line[MAX_LINE];
                    while (fgets(line, sizeof(line), file) != NULL) {
                        if (strstr(line, "SUSPICIOUS") != NULL) {
                            int hapus = remove(filePath);
                            if (hapus == 0) {
                                write_log(filePath);
                            } else {
                                perror("remove");
                            }
                            break;
                        }
                    }

                    // program selesai terbaca
                    fclose(file);
                } else {
                    perror("fopen");
                }
            }
        }
        closedir(dir);
        sleep(30);
    }
}

// fungsi handle sinyal
void signal_handler(int signo) {
    if (signo == SIGINT || signo == SIGTERM) {
        printf("dihandle dulu \n");
        exit(EXIT_SUCCESS);
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("format route path: %s '/home/bhiss/BHISMA_KULIAH/SISOP/sisop-praktikum-modul-2-2023-mh-it10/soal_1'\n", argv[0]);
        return 1;
    }

    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);

    // Mengecek apakah cleaner.log sudah ada
    FILE *LogFile = fopen("/home/bhiss/BHISMA_KULIAH/SISOP/sisop-praktikum-modul-2-2023-mh-it10/soal_1/cleaner.log", "a");
    if (LogFile == NULL) {
        // Jika cleaner.log belum ada, maka buat file tersebut
        LogFile = fopen("/home/bhiss/BHISMA_KULIAH/SISOP/sisop-praktikum-modul-2-2023-mh-it10/soal_1/cleaner.log", "w");
        fclose(LogFile);
    } else {
        fclose(LogFile);
    }

    // Proses child dengan menggunakan fork
    pid_t child_pid = fork(); 

    if (child_pid == 0) {
        // Menggunakan daemon ketika dijalankan oleh proses child 
        daemon(1, 1);
        // Jika kondisi child_pid == 0 juga jalankan fungsi cleaner dengan argumen tersebut
        cleaner(argv[1]); 
    } else if (child_pid > 0) {
        // Jika child_pid > 0 maka jalankan parent proses
        // Ketika parent proses selesai, child process akan menjadi daemon
        exit(0);
    } else {
        // Jika fork gagal
        fprintf(stderr, "Forknya gagal kak\n");
        return 1;
    }

    return 0;
}
