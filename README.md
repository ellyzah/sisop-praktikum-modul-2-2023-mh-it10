## Description

Repository untuk menyimpan **code jawaban praktikum kelompok 10** dari soal-soal Sistem Operasi 2023/2024

Untuk mengakses keperluan, silakan klik **[di sini](https://sisop23.carrd.co)**.

## Praktikan

- [Bhisma Elki Pratama](https://github.com/bhismapratama)
- [Siti Nur Ellyzah](https://github.com/ellyzah)
- [Bintang Ryan Wardana](https://github.com/bintangryan)

main contribution from **[@LABKCKSITS](https://github.com/lab-KCKS)**.

# sisop-praktikum-modul-2-2023-MH-IT10

## Anggota Kelompok

|    NRP     |         Nama          |
| :--------: | :-------------------: |
| 5027221005 |  Bhisma Elki Pratama  |
| 5027221014 |   Siti Nur Ellyzah    |
| 5027221022 | Bintang Ryan Wardhana |


# Soal Shift Modul 2

**Sistem Operasi 2023**

### Petunjuk Umum

1. Waktu pengerjaan dimulai Senin (2/10) setelah sesi lab hingga Sabtu (7/10) pukul 22.00 WIB.
2. Praktikan diharapkan membuat laporan penjelasan dan penyelesaian soal dalam bentuk README di GitLab.
3. Format nama repository di GitLab: `sisop-praktikum-modul-[Nomor Modul]-2023-[Kode Dosen Kelas]-[Nama Kelompok]` (contoh: `sisop-praktikum-modul-2-2023-MH-IT01`).
4. Struktur repository harus seperti berikut:

```
├── soal_1
│   ├── cleaner.c
├── soal_2
│   ├── cavs.c
├── soal_3
│   ├── lukisan.c
├── soal_4
│   ├── antivirus.c
```

> Jika melanggar struktur repo akan dianggap sama dengan curang dan menerima konsekuensi sama dengan melakukan kecurangan.

5. Setelah pengerjaan selesai, semua script bash, awk, dan file yang berisi cron job ditaruh di GitLab masing-masing kelompok, dan link GitLab diletakkan pada formulir yang disediakan. Pastikan GitLab diatur ke publik.
6. Commit terakhir maksimal 10 menit setelah waktu pengerjaan berakhir. Jika melewati waktu, nilai akan dinilai berdasarkan commit terakhir.
7. Jika tidak ada pengumuman perubahan soal oleh asisten, maka soal dianggap dapat diselesaikan.
8. Jika ditemukan soal yang tidak dapat diselesaikan, harap mencatatnya pada README beserta permasalahan yang ditemukan.
9. Praktikan tidak diperbolehkan menanyakan jawaban dari soal yang diberikan kepada asisten maupun praktikan dari kelompok lainnya.
10. Jika ditemukan indikasi kecurangan dalam bentuk apapun dalam pengerjaan soal shift, maka nilai dianggap 0.
11. Pengerjaan soal shift sesuai dengan modul yang telah diajarkan.
12. Zip dari repository dikirim ke email asisten penguji dengan subjek yang sama dengan nama judul repository, dikirim sebelum deadline dari soal shift.
13. Jika terdapat revisi soal akan dituliskan pada halaman terakhir README.

---

### Soal 1

John adalah seorang mahasiswa biasa. Pada tahun ke-dua kuliahnya, dia merasa bahwa dia telah menyianyiakan waktu kuliahnya selama ini. Selama mengerjakan tugas, John selalu menggunakan bantuan ChatGPT dan tidak pernah mempelajari apapun dari hal tersebut. Untuk itu, pada soal kali ini John bertekad untuk tidak menggunakan ChatGPT dan mencoba menyelesaikan tugasnya dengan tangan dan pikirannya sendiri.

Melihat tekad yang kuat dari John, Mark, dosen yang mengajar John, ingin membantunya belajar dengan memberikan sebuah ujian. Sebelum memberikan ujian pada John, Mark berpesan bahwa John harus bersungguh-sungguh dalam mengerjakan ujian, fokus untuk belajar, dan tidak perlu khawatir akan nilai yang diberikan. Mark memberikan ujian pada John untuk membuatkannya sebuah program `cleaner` sederhana dengan ketentuan berikut:

a. Program tersebut menerima input path dengan menggunakan `argv`.
b. Program tersebut bertugas untuk menghapus file yang didalamnya terdapat string "SUSPICIOUS" pada direktori yang telah diinputkan oleh user.
c. Program tersebut harus berjalan secara daemon.
d. Program tersebut akan terus berjalan di background dengan jeda 30 detik.
e. Dalam pembuatan program tersebut, tidak diperbolehkan menggunakan fungsi `system()`.
f. Setiap kali program tersebut menghapus sebuah file, maka akan dicatat pada file 'cleaner.log' yang ada pada direktori home user dengan format seperti berikut: `[YYYY-mm-dd HH:MM:SS] '<absolute_path_to_file>' has been removed.`

#### Contoh Penggunaan:

```sh
./cleaner '/home/user/cleaner'
```

#### Contoh cleaner.log:

```
[2023-10-02 18:54:20] '/home/user/cleaner/is.txt' has been removed.
[2023-10-02 18:54:20] '/home/user/cleaner/that.txt' has been removed.
[2023-10-02 18:54:50] '/home/user/cleaner/who.txt' has been removed.
```

#### Contoh Valid Sample File:

```
LJuUHmAnVLLCMRhLTcqy
bBpSUSPICIOUSagQKmLA
BitSuQNHSLmZDvEcvbGc
```

#### Contoh Invalid Sample File:

```
LJuUHmAnVLLCMRhLTcqy
TFvefehhpWDCbkdirmlh
BitSuQNHSLmZDvEcvbGc
```

---

### Soal 2

QQ adalah fan Cleveland Cavaliers. Ia ingin memajang kamarnya dengan poster foto roster Cleveland Cavaliers tahun 2016. Maka yang dia lakukan adalah meminta tolong temannya yang sangat sisopholic untuk membuatkannya sebuah program untuk mendownload gambar-gambar pemain tersebut. Sebagai teman baiknya, bantu QQ untuk mencarikan foto foto yang dibutuhkan QQ dengan ketentuan sebagai berikut:

1. Buatlah program bernama `cavs.c` yang dimana program tersebut akan membuat folder `players`.
2. Program akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract `players.zip` ke dalam folder players yang telah dibuat. Lalu hapus file zip tersebut agar tidak memenuhi komputer QQ.
3. Dikarenakan database yang diunduh masih data mentah. Maka bantulah QQ untuk menghapus semua pemain yang bukan dari Cleveland Cavaliers yang ada di directory.
4. Setelah mengetahui nama-nama pemain Cleveland Cavaliers, QQ perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 5 proses yang berbeda. Untuk kategori folder akan menjadi 5 yaitu point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C).
5. Hasil kategorisasi akan di outputkan ke file `Formasi.txt`, dengan berisi:

```
```
PG: {jumlah pemain}
SG: {jumlah pemain}
SF: {jumlah pemain}
PF: {jumlah pemain}
C: {jumlah pemain}
```

6. Ia ingin memajang foto pemain yang menembakkan game-winning shot pada ajang NBA Finals 2016, tepatnya pada game 7, dengan membuat folder `clutch`, yang di dalamnya berisi foto pemain yang bersangkutan.
7. Ia merasa kurang lengkap jika tidak memajang foto pemain yang melakukan The Block pada ajang yang sama. Maka dari itu, ditaruhlah foto pemain tersebut di folder `clutch` yang sama.

Catatan:

- Format nama file yang akan diunduh dalam zip berupa `[tim]-[posisi]-[nama].png`.
- Tidak boleh menggunakan `system()`. Gunakan `exec()` dan `fork()`.
- Directory `.` dan `..` tidak termasuk yang akan dihapus.
- 2 poin soal terakhir dilakukan setelah proses kategorisasi selesai.

---

### Soal 3

Albedo adalah seorang seniman terkenal dari Mondstadt. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini Albedo sedang menghadapi creativity block. Sebagai teman berkebangsaan dari Fontaine yang jago sisop, bantu Albedo untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi!

a. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp `[YYYY-MM-dd_HH:mm:ss]`.
b. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://source.unsplash.com/{widthxheight}, dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran `(t%1000)+50` piksel dimana `t` adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp `[YYYY-mm-dd_HH:mm:ss]`.
c. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete (sehingga hanya menyisakan .zip, format nama `[YYYY-mm-dd_HH:mm:ss].zip` tanpa `[]`).
d. Karena takut program tersebut lepas kendali, Albedo ingin program tersebut meng-generate sebuah program "killer" yang siap di run (executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
e. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. Untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen `-a`. Untuk MODE_B, program harus dijalankan dengan argumen `-b`. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai (semua folder terisi gambar, ter-zip lalu di delete).

Catatan:

- Tidak boleh menggunakan `system()`.
- Proses berjalan secara daemon.
- Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping).

---

### Soal 4

Choco adalah seorang ahli pertahanan siber yang tidak suka memakai ChatGPT dalam menyelesaikan masalah. Dia selalu siap melindungi data dan informasi dari ancaman dunia maya. Namun, kali ini, dia membutuhkan bantuan Anda untuk meningkatkan kinerja antivirus yang telah dia buat sebelumnya.

A. Bantu Choco dalam mengoptimalkan program antivirus bernama `antivirus.c`. Program ini seharusnya dapat memeriksa file di folder `sisop_infected`, dan jika file tersebut diidentifikasi sebagai virus berdasarkan ekstensinya, program harus memindahkannya ke folder `quarantine`. List dari format ekstensi/tipe file nya bisa didownload di [Link Ini](http://link.ke/file/format/extensions.csv), proses mendownload tidak boleh menggunakan `system()`. Daftar ekstensi file yang dianggap virus tersimpan dalam file `extensions.csv`.

B. Ada kejutan di dalam file `extensions.csv`. Hanya 8 baris pertama yang tidak dienkripsi. Baris-baris setelahnya perlu Anda dekripsi menggunakan algoritma rot13 untuk mengetahui ekstensi virus lainnya. Setiap kali program mendeteksi file virus, catatlah informasi tersebut di `virus.log`. Format log harus sesuai dengan:

```
[nama_user][Dd-Mm-Yy:Hh-Mm-Ss] - {nama file yang terinfeksi} - {tindakan yang diambil}
```

Contoh:  

```
[sisopUser][29-09-23:08-59-01] - test.locked - Moved to quarantine
```

C. Dunia siber tidak pernah tidur, dan demikian juga virus. Choco memerlukan antivirus yang terus berjalan di latar belakang tanpa harus dia intervensi. Dengan menjalankan program ini sebagai Latar belakang, program akan secara otomatis memeriksa folder `sisop_infected` setiap detik.

D. Choco juga membutuhkan level-level keamanan antivrus jadi dia membuat 3 level yaitu low, medium, hard. Argumen tersebut di pakai saat menjalankan antivirus.

- Low: Hanya me-log file yg terdeteksi
- Medium: log dan memindahkan file yang terdeteksi
- Hard: log dan menghapus file yang terdeteksi

ex: ./antivirus -p low

Kadang-kadang, Choco mungkin perlu mengganti level keamanan dari antivirus tanpa harus menghentikannya. Integrasikan kemampuan untuk mengganti level keamanan antivirus dengan mengirim sinyal ke daemon. Misalnya, menggunakan SIGUSR1 untuk mode "low", SIGUSR2 untuk "medium", dan SIGRTMIN untuk mode "hard".

Contoh:

```
kill -SIGUSR1 <pid_program>
```

Meskipun penting untuk menjalankan antivirus, ada saat-saat Choco mungkin perlu menonaktifkannya sementara. Bantu dia dengan menyediakan fitur untuk mematikan antivirus dengan cara yang aman dan efisien.


## PENJELASAN JAWABAN

### SOAL NO 1 (cleaner.c)

```c
// initialisasi library-library yand diperlukan

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>

// deklarasi dulu buat banyaknya character
#define MAX_PATH 260
#define MAX_LINE 256

void write_log(const char *filePath) {
    // pertama membuat proses child dengan fork()
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    // menggunakan pengkondisian pada proses child
    if (pid == 0) {
        time_t waktu;
        struct tm *detail_waktu;
        char waktu_string[20];
        
        time(&waktu);
        detail_waktu = localtime(&waktu);
        strftime(waktu_string, sizeof(waktu_string), "%Y-%m-%d %H:%M:%S", detail_waktu);

        char LogEntry[200];
        snprintf(LogEntry, sizeof(LogEntry), "[%s] '%s' dihapus kak.\n", waktu_string, filePath);

        FILE *LogFile = fopen("/home/bhiss/BHISMA_KULIAH/SISOP/sisop-praktikum-modul-2-2023-mh-it10/soal_1/cleaner.log", "a");
        if (LogFile != NULL) {
            fputs(LogEntry, LogFile);
            fclose(LogFile);
        }

        // merupakan syntax untuk keluar dari proses child
        exit(EXIT_SUCCESS); 
    } else { 
        // jika pid != 0 maka dilakukan parent proses
        // deklarasi variable
        int status;
        // menunggu proses child selesai
        waitpid(pid, &status, 0); 
    }
}

// membuat fungsi sesuai arahan dari soal (cleaner)
void cleaner(const char *dir_path) {
    while (1) {
        // Buka direktori yang dituju dengan pointer *dir
        DIR *dir = opendir(dir_path);

        if (!dir) {
            perror("opendir");
            return;
        }

        struct dirent *entry;

        while ((entry = readdir(dir)) != NULL) {
            // Check apakah entri adalah direktori
            if (entry->d_type == DT_DIR) {
                // Jangan memproses entri "." dan ".."
                if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                    // Gabungkan dir path dengan nama direktori
                    char sub_dir_path[MAX_PATH];
                    snprintf(sub_dir_path, sizeof(sub_dir_path), "%s/%s", dir_path, entry->d_name);

                    // Proses direktori sub dengan memanggil cleaner rekursif
                    cleaner(sub_dir_path);
                }
            } else if (entry->d_type == DT_REG) {
                char filePath[MAX_PATH];
                snprintf(filePath, sizeof(filePath), "%s/%s", dir_path, entry->d_name);

                FILE *file = fopen(filePath, "r");

                if (file) {
                    char line[MAX_LINE];
                    while (fgets(line, sizeof(line), file) != NULL) {
                        if (strstr(line, "SUSPICIOUS") != NULL) {
                            int hapus = remove(filePath);
                            if (hapus == 0) {
                                write_log(filePath);
                            } else {
                                perror("remove");
                            }
                            break;
                        }
                    }

                    // program selesai terbaca
                    fclose(file);
                } else {
                    perror("fopen");
                }
            }
        }
        closedir(dir);
        sleep(2);
    }
}

// fungsi handle sinyal
void signal_handler(int signo) {
    if (signo == SIGINT || signo == SIGTERM) {
        printf("dihandle dulu \n");
        exit(EXIT_SUCCESS);
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("format route path: %s '/home/bhiss/BHISMA_KULIAH/SISOP/sisop-praktikum-modul-2-2023-mh-it10/soal_1'\n", argv[0]);
        return 1;
    }

    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);

    // Mengecek apakah cleaner.log sudah ada
    FILE *LogFile = fopen("/home/bhiss/BHISMA_KULIAH/SISOP/sisop-praktikum-modul-2-2023-mh-it10/soal_1/cleaner.log", "a");
    if (LogFile == NULL) {
        // Jika cleaner.log belum ada, maka buat file tersebut
        LogFile = fopen("/home/bhiss/BHISMA_KULIAH/SISOP/sisop-praktikum-modul-2-2023-mh-it10/soal_1/cleaner.log", "w");
        fclose(LogFile);
    } else {
        fclose(LogFile);
    }

    // Proses child dengan menggunakan fork
    pid_t child_pid = fork(); 

    if (child_pid == 0) {
        // Menggunakan daemon ketika dijalankan oleh proses child 
        daemon(1, 1);
        // Jika kondisi child_pid == 0 juga jalankan fungsi cleaner dengan argumen tersebut
        cleaner(argv[1]); 
    } else if (child_pid > 0) {
        // Jika child_pid > 0 maka jalankan parent proses
        // Ketika parent proses selesai, child process akan menjadi daemon
        exit(0);
    } else {
        // Jika fork gagal
        fprintf(stderr, "Forknya gagal kak\n");
        return 1;
    }

    return 0;
}

```


**Deskripsi Program**

Program yang diberikan pada soal adalah sebuah program *cleaner* yang memeriksa file di dalam direktori tertentu yang nantinya menghapus file-file yang mengandung kata / string "SUSPICIOUS". Jika ada file ynag dihapus maka informasi log akan dicatat ke dalam file `cleaner.log`.

**Kumpulan Library dan Header**

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
```

**Fungsi `write_log`**

```c
void write_log(const char *filePath) {
    // ...
}
```

Fungsi (void) ini digunakan untuk mencatat informasi log setiap kali file terhapus. Informasi yang dicatat mencakup waktu penghapusan dan route path file yang dihapus. Informasi log tersebut dicatat dalam file `cleaner.log`.

Penjelasan dari setiap bagian kode:

1. `void write_log(const char *filePath) {`: 
   - Fungsi `write_log` menerima satu argumen, yaitu `filePath` yang merupakan string berisi path dari file yang akan dicatat.

2. `pid_t pid = fork();`:
   - Di sini, program mencoba membuat proses baru dengan memanggil fungsi `fork()`. Hasil dari `fork()` disimpan dalam variabel `pid_t pid`. Hasil `fork()` akan berbeda pada parent dan child proses.

3. `if (pid == -1) { ... }`:
   - Jika `fork()` mengembalikan nilai -1, artinya terdapat kesalahan dalam pembuatan proses baru. Pesan kesalahan akan dicetak menggunakan `perror("fork")` dan program akan memberikan output `exit(EXIT_FAILURE)`.

4. `if (pid == 0) { ... } else { ... }`:
   - Di sini, program melakukan pengkondisian berdasarkan nilai `pid`. Jika `pid` adalah 0, maka merupakan proses child. Jika tidak, maka merupakan proses parent.

5. Pada blok `if (pid == 0) { ... }` (proses child):
   - `time_t waktu;`, `struct tm *detail_waktu;`, `char waktu_string[20];`:
     - Variabel ini digunakan untuk menyimpan informasi waktu.
   - `time(&waktu);`, `detail_waktu = localtime(&waktu);`, `strftime(waktu_string, sizeof(waktu_string), "%Y-%m-%d %H:%M:%S", detail_waktu);`:
     - Program mengambil waktu saat ini dan memformatnya menjadi string dengan format "YYYY-MM-DD HH:MM:SS". Hasilnya disimpan dalam `waktu_string`.
   - `char LogEntry[200]; snprintf(LogEntry, sizeof(LogEntry), "[%s] '%s' has been removed.\n", waktu_string, filePath);`:
     - String `LogEntry` akan berisi informasi log yang akan dicatat. Informasi ini mencakup waktu dan path file yang dihapus.
   - `FILE *LogFile = fopen("/home/bhiss/BHISMA_KULIAH/SISOP/sisop-praktikum-modul-2-2023-mh-it10/soal_1/cleaner.log", "a");`:
     - Program mencoba membuka file log untuk ditambahkan. Jika file tidak dapat dibuka, `LogFile` bernilai `NULL`.
   - `if (LogFile != NULL) { ... }`:
     - Jika file log berhasil dibuka, maka `LogEntry` akan ditulis ke dalam file log menggunakan `fputs(LogEntry, LogFile);` dan file akan ditutup dengan `fclose(LogFile);`.

   - `exit(EXIT_SUCCESS);`:
     - Digunakan untuk mengakhiri proses child.

6. Pada blok `else` (proses parent):
   - `int status;`:
     - Variabel ini akan digunakan menyimpan status dari proses child.
   - `waitpid(pid, &status, 0);`:
     - Proses parent akan menunggu proses child selesai. `waitpid` adalah fungsi yang digunakan untuk menunggu proses tertentu selesai. `pid` adalah ID dari proses yang akan ditunggu, `&status` adalah alamat dari variabel yang akan menyimpan status dari proses child, dan `0` adalah opsi tambahan.

**Fungsi `cleaner`**

```c
void cleaner(const char *dir_path) {
    // ...
}
```

Fungsi (void) cleaner merupakan inti dari program soal 1 modul 2 ini. Fungsi tersebut membuka direktori yang ditentukan oleh `dir_path` dan melakukan pemeriksaan terhadap setiap file di dalamnya. Jika file berisi kata/string "SUSPICIOUS", maka file tersebut akan dihapus dan informasi log akan dicatat.

Fungsi cleaner bersifat rekursif, dengan arti jika menemukan direktori dalam direktori yang sedang diperiksa, maka fungsinya akan memanggil dirinya sendiri untuk memeriksa direktori tersebut.

### Penjelasan tiap bagian kode

1. `void cleaner(const char *dir_path) { ... }`:
   - Ini adalah deklarasi dan implementasi dari fungsi `cleaner`. Fungsi ini menerima satu argumen, yaitu `dir_path` yang merupakan string yang berisi path dari direktori yang akan diperiksa.

2. `while (1) { ... }`:
   - Ini adalah loop yang akan terus berjalan selama program aktif.

3. `DIR *dir = opendir(dir_path);`:
   - Membuka direktori yang dituju dengan menggunakan `opendir` dan menyimpan hasilnya dalam pointer `*dir`.

4. `if (!dir) { ... }`:
   - Jika `opendir` gagal (mengembalikan nilai `NULL`), maka program akan mencetak pesan kesalahan menggunakan `perror("opendir")` dan fungsi selesai.

5. `struct dirent *entry;`:
   - Mendeklarasikan sebuah pointer `entry` yang akan digunakan untuk menyimpan informasi entri di dalam direktori.

6. `while ((entry = readdir(dir)) != NULL) { ... }`:
   - Loop akan berjalan selama masih ada entri di dalam direktori. Setiap entri akan diperiksa.

7. `if (entry->d_type == DT_DIR) { ... } else if (entry->d_type == DT_REG) { ... }`:
   - Memeriksa jenis dari entri, apakah itu direktori (`DT_DIR`) atau file (`DT_REG`).

8. Jika jenis entri adalah direktori:
   - `if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) { ... }`:
     - Memastikan bahwa entri bukan "." (diri sendiri) atau ".." (parent directory).
     - Membuat path lengkap ke subdirektori dengan menggabungkan `dir_path` dengan nama subdirektori.
     - Memanggil fungsi `cleaner` secara rekursif untuk memeriksa subdirektori.

9. Jika jenis entri adalah file:
   - `char filePath[MAX_PATH]; snprintf(filePath, sizeof(filePath), "%s/%s", dir_path, entry->d_name);`:
     - Membuat path lengkap ke file dengan menggabungkan `dir_path` dengan nama file.

   - `FILE *file = fopen(filePath, "r");`:
     - Membuka file untuk dibaca.

   - `if (file) { ... } else { perror("fopen"); }`:
     - Jika file berhasil dibuka, program akan membaca setiap baris di dalamnya. Jika tidak, akan mencetak pesan kesalahan menggunakan `perror("fopen")`.

   - `while (fgets(line, sizeof(line), file) != NULL) { ... }`:
     - Membaca setiap baris dari file.

   - `if (strstr(line, "SUSPICIOUS") != NULL) { ... }`:
     - Memeriksa apakah baris mengandung teks "SUSPICIOUS".

   - Jika ada "SUSPICIOUS":
     - `int hapus = remove(filePath);`:
       - Menghapus file menggunakan `remove` dan menyimpan hasilnya dalam variabel `hapus`.

     - `if (hapus == 0) { ... } else { perror("remove"); }`:
       - Jika penghapusan berhasil (`hapus` sama dengan 0), maka fungsi `write_log` akan dipanggil untuk mencatat penghapusan. Jika tidak, akan mencetak pesan kesalahan menggunakan `perror("remove")`.

11. `fclose(file);`:
    - Menutup file setelah selesai membacanya.

12. `closedir(dir);`:
    - Menutup direktori setelah selesai memproses entri-entri di dalamnya.

13. `sleep(30);`:
    - Program akan "tidur" selama 30 detik sebelum memeriksa kembali direktori untuk mengatur frekusendi pemindaian.


**Fungsi `signal_handler`**

```c
void signal_handler(int signo) {
    // ...
}
```

Fungsi signal ini bertujuan sebagai penangan signal (catatan signal modul 2). Jika program menerima sinyal SIGINT (Ctrl+C) atau SIGTERM, program akan mencetak pesan dan berakhir tanpa ada error atau salah eksekusi.

### Penjelasan dari bagian kode

1. ```c
   void signal_handler(int signo) {
   ```
   - Ini adalah deklarasi dan implementasi dari fungsi `signal_handler`. Fungsi ini menerima satu argumen, yaitu `signo`, yang merupakan kode sinyal yang diterima.

2. ```c
   if (signo == SIGINT || signo == SIGTERM) {
   ```
   - Memeriksa apakah sinyal yang diterima adalah `SIGINT` (Ctrl+C) atau `SIGTERM` (meminta program untuk berhenti).

3. ```c
   printf("dihandle dulu \n");
   ```
   - Mencetak pesan "dihandle dulu" ke console. Ini adalah pesan yang akan dicetak jika fungsi menerima sinyal `SIGINT` atau `SIGTERM`.

4. ```c
   exit(EXIT_SUCCESS);
   ```
   - Mengakhiri program dengan status berhasi (`EXIT_SUCCESS`). Program selesai dan normal

**Fungsi `main`**

```c
int main(int argc, char *argv[]) {
    // ...
}
```

Fungsi `int main` merupaakan program utama yang apabila apapun yang dimasukkan dalam fungsi `main` akan berpengaruh juga terhadap program yang berjalan. Pertama, memeriksa apakah argumen yang diberikan sesuai. Kemudian menyiapkan penanganan sinyal dan mencoba untuk membuat child process menggunakan `fork`. Child process kemudian diubah menjadi daemon menggunakan `daemon(1, 1)` untuk menjalankan fungsi `cleaner`. Parent process keluar setelah membuat child process. Terdapat kondisi jika `fork` gagal, pesan kesalahan akan dicetak.

### Penjelasan dari tiap bagian

1. **Memproses argumen dari command line**:

    ```c
    if (argc != 2) {
        printf("format route path: %s '/home/bhiss/BHISMA_KULIAH/SISOP/sisop-praktikum-modul-2-2023-mh-it10/soal_1'\n", argv[0]);
        return 1;
    }
    ```

    - Program ini memeriksa jumlah argumen yang diberikan di perintah. Jika jumlahnya bukan 2 (artinya hanya argumen program dan path direktori yang diinginkan), maka program akan mencetak pesan tentang format yang benar dan kemudian keluar dengan status 1.

2. **Atur Sinyal**:

    ```c
    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);
    ```

    - Program ini menggunakan fungsi `signal` untuk menangani sinyal `SIGINT` (Ctrl+C) dan `SIGTERM`. Fungsi `signal_handler` akan dijalankan jika program menerima sinyal ini.

3. **Membuat atau memeriksa adanya file log**:

    ```c
    FILE *LogFile = fopen("/home/bhiss/BHISMA_KULIAH/SISOP/sisop-praktikum-modul-2-2023-mh-it10/soal_1/cleaner.log", "a");
    if (LogFile == NULL) {
        LogFile = fopen("/home/bhiss/BHISMA_KULIAH/SISOP/sisop-praktikum-modul-2-2023-mh-it10/soal_1/cleaner.log", "w");
        fclose(LogFile);
    } else {
        fclose(LogFile);
    }
    ```

    - Program mencoba membuka file log (`cleaner.log`) dalam mode append ("a"). Jika file tidak dapat dibuka (kemungkinan belum ada), maka program akan mencoba membuat file log baru (`"w"` mode) dan kemudian menutupnya. Jika file log sudah ada, program akan langsung menutupnya.

4. **Membuat Proses Child dengan `fork`**:

    ```c
    pid_t child_pid = fork(); 

    if (child_pid == 0) {
        daemon(1, 1);
        cleaner(argv[1]); 
    } else if (child_pid > 0) {
        exit(0);
    } else {
        fprintf(stderr, "Forknya gagal kak\n");
        return 1;
    }
    ```

    - Program ini membuat proses baru (child) menggunakan fungsi `fork`. Jika `fork` berhasil, maka di dalam child process, program akan menggunakan fungsi `daemon` untuk menjadikannya sebagai daemon. Selanjutnya, program akan memanggil fungsi `cleaner` dengan argumen yang diberikan pada baris perintah.
    - Di dalam parent process, program akan langsung keluar dengan status 0 jadinya proses parent selesai.

    - Jika `fork` gagal, program akan mencetak pesan kesalahan.

5. **Kembali dari `main`**:

    ```c
    return 0;
    ```

    - Program akan keluar dari fungsi `main` dengan status 0, program selesai dengan sukses.

**Cara Penggunaan:**

1. Run `gcc cleaner.c -o cleaner`,
2. Kemudian run program dengan memberikan path direktori sebagai argumen di terminal atau command line. contoh: `./program /path/ke/direktori_utama`.

## DOCUMENTASI

![no 1 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/docum%20soal_1%20lagi.png)
![no 1 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/docum%20soal_1.png)




## PENJELASAN JAWABAN

### SOAL NO 2 (cavs.c)


Arahan soal yang diberikan memberikan penjelasan program dalam bahasa C yang melakukan beberapa operasi file dan folder. Program ini terkait dengan pengelolaan data pemain basket tim cleveland cavaliers.

### Penjelasan Fungsinya

1. #### `buatFolder(char *folderName)`
    - Fungsi ini membuat folder dengan nama dalam parameter `folderName`.
    - Proses ini dilakukan dengan melakukan fork, dan di dalam child process, menggunakan `execv` untuk menjalankan perintah `mkdir`.

### Penjelasan tiap bagian

1. ```c
   void buatFolder(char *folderName) {
   ```
   - Ini adalah deklarasi dan implementasi dari fungsi `buatFolder`. Fungsi ini menerima satu argumen, yaitu `folderName`, yang merupakan string yang berisi nama folder yang akan dibuat.

2. ```c
   pid_t child = fork();
   ```
   - Membuat proses child dengan menggunakan `fork()`. Hasil dari `fork()` disimpan dalam variabel `child`.

3. ```c
   if (child == 0) {
       // buat folder dulu
       char *argv[] = {"mkdir", "-p", folderName, NULL};
       execv("/bin/mkdir", argv);
   }
   ```
   - Di dalam proses child (ketika nilai `child` adalah 0), program akan menjalankan perintah `mkdir` untuk membuat folder baru.
     - `char *argv[] = {"mkdir", "-p", folderName, NULL};`: Membuat array dari string yang akan digunakan sebagai argumen untuk perintah `execv`. Argumen pertama adalah "mkdir", yang kedua adalah opsi `-p` (untuk membuat folder beserta sub-folder jika belum ada), dan terdapat nama folder (`folderName`).
     - `execv("/bin/mkdir", argv);`: Menggantikan proses saat ini dengan proses `mkdir` menggunakan `execv`.

4. ```c
   wait(NULL);
   ```
   - Proses parent akan menunggu hingga proses child selesai. Ini memastikan bahwa folder baru sudah selesai dibuat sebelum program melanjutkan eksekusi.

2. #### `unduhDatabasePemain()`
    - Fungsi ini mengunduh data pemain dari suatu URL menggunakan perintah `wget`.
    - Proses ini juga dilakukan dengan melakukan fork, dan di dalam child process, menggunakan `execv` untuk menjalankan perintah `wget`.

### Penjelasan tiap bagian

1. ```c
   void unduhDatabasePemain() {
   ```
   - Ini adalah deklarasi dan implementasi dari fungsi `unduhDatabasePemain`.

2. ```c
   pid_t child = fork();
   ```
   - Membuat proses child dengan menggunakan `fork()`. Hasil dari `fork()` disimpan dalam variabel `child`.

3. ```c
   if (child == 0) {
       // lakukan get download untuk dapetin data pemainnya
       char *argv[] = {"wget", "--no-check-certificate", "https://drive.google.com/u/0/uc?id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK&export=download", "-O", "pemain.zip", NULL};
       execv("/usr/bin/wget", argv);
   }
   ```
   - Di dalam proses child (ketika nilai `child` adalah 0), program menggunakan perintah `wget` untuk mengunduh database pemain dari URL yang diberikan.
     - `char *argv[] = {"wget", "--no-check-certificate", "https://drive.google.com/u/0/uc?id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK&export=download", "-O", "pemain.zip", NULL};`: Membuat array dari string yang akan digunakan sebagai argumen erintah `execv`. Argumen tersebut berisi:
       - "wget": Perintah yang akan dijalankan.
       - "--no-check-certificate": Opsi mengabaikan sertifikat SSL.
       - URL yang menunjuk ke file yang akan diunduh.
       - "-O": Opsi untuk menentukan nama file output (dalam hal ini, "pemain.zip").
       - NULL: Akhir aargumen.

     - `execv("/usr/bin/wget", argv);`: Menggantikan proses saat ini dengan proses `wget` menggunakan `execv`.

4. ```c
   wait(NULL);
   ```
   - Proses parent akan menunggu hingga proses child selesai.

3. #### `ekstrakDatabase()`
    - Fungsi ini mengekstrak data pemain dari file zip `pemain.zip`.
    - Proses ini juga dilakukan dengan melakukan fork, dan di dalam child process, menggunakan `execv` untuk menjalankan perintah `unzip`.

### Penjelasan tiap bagian

1. ```c
   void ekstrakDatabase() {
   ```
   - Ini adalah deklarasi dan implementasi dari fungsi `ekstrakDatabase`.

2. ```c
   pid_t child = fork();
   ```
   - Membuat proses child dengan menggunakan `fork()`. Hasil dari `fork()` disimpan dalam variabel `child`.

3. ```c
   if (child == 0) {
       // perunzipan duniawi
       char *argv[] = {"unzip", "pemain.zip", "-d", "players", NULL};
       execv("/usr/bin/unzip", argv);
   }
   ```
   - Di dalam proses child (ketika nilai `child` adalah 0), program akan menggunakan perintah `unzip` untuk mengekstrak file `pemain.zip`.
     - `char *argv[] = {"unzip", "pemain.zip", "-d", "players", NULL};`: Membuat array dari string yang akan digunakan sebagai argumen untuk perintah `execv`. Argumen berisi:
       - "unzip": Perintah yang akan dijalankan.
       - "pemain.zip": Nama file yang akan diekstrak.
       - "-d": Opsi menentukan direktori tempat hasil ekstraksi yang akan ditempatkan.
       - "players": Nama direktori tujuan ekstraksi.
       - NULL: Akhir dari argumen.

     - `execv("/usr/bin/unzip", argv);`: Menggantikan proses saat ini dengan proses `unzip` menggunakan `execv`. 

4. ```c
   wait(NULL);
   ```
   - Proses parent akan menunggu hingga proses child selesai.

4. #### `hapusDatabaseZip()`
    - Fungsi ini menghapus file zip `pemain.zip` setelah data pemain diekstrak.
    - Proses ini juga dilakukan dengan melakukan fork, dan di dalam child process, menggunakan `execv` untuk menjalankan perintah `rm`.

### Penjelasan tiap bagian

1. ```c
   void hapusDatabaseZip() {
   ```
   - Ini adalah deklarasi dan implementasi dari fungsi `hapusDatabaseZip`.

2. ```c
   pid_t child = fork();
   ```
   - Membuat proses child dengan menggunakan `fork()`. Hasil dari `fork()` disimpan dalam variabel `child`.

3. ```c
   if (child == 0) {
       // hapus pemain.zip yang udah ga kepakai biar ga menuhin storage juga
       char *argv[] = {"rm", "pemain.zip", NULL};
       execv("/bin/rm", argv);
   }
   ```
   - Di dalam proses child (ketika nilai `child` adalah 0), program akan menggunakan perintah `rm` untuk menghapus file `pemain.zip`.
     - `char *argv[] = {"rm", "pemain.zip", NULL};`: Membuat array dari string yang digunakan sebagai argumen untuk perintah `execv`. Argumen berisi:
       - "rm": Perintah yang akan dijalankan.
       - "pemain.zip": Nama file yang akan dihapus.
       - NULL: Akhir dari argumen.

     - `execv("/bin/rm", argv);`: Menggantikan proses saat ini dengan proses `rm` menggunakan `execv`. 

4. ```c
   wait(NULL);
   ```
   - Proses parent akan menunggu hingga proses child selesai.

5. #### `hapusPemainSelainCavs()`
    - Fungsi ini mencari dan menghapus file pemain yang bukan dari tim Cavaliers.
    - Proses ini juga dilakukan dengan melakukan fork, dan di dalam child process, menggunakan `execv` untuk menjalankan perintah `find`.

### Penjelasan tiap bagian

1. ```c
   void hapusPemainSelainCavs() {
   ```
   - Ini adalah deklarasi dan implementasi dari fungsi `hapusPemainSelainCavs`.

2. ```c
   pid_t child = fork();
   ```
   - Membuat proses child dengan menggunakan `fork()`. Hasil dari `fork()` disimpan dalam variabel `child`.

3. ```c
   if (child == 0) {
       // proses menemukan cavaliers
       char *argv[] = {"find", "players", "-type", "f", "!", "-name", "*Cavaliers*.png", "-delete", NULL};
       execv("/usr/bin/find", argv);
   }
   ```
   - Di dalam proses child (ketika nilai `child` adalah 0), program akan menggunakan perintah `find` untuk mencari dan menghapus gambar pemain yang bukan dari tim Cavaliers.
     - `char *argv[] = {"find", "players", "-type", "f", "!", "-name", "*Cavaliers*.png", "-delete", NULL};`: Membuat array dari string yang akan digunakan sebagai argumen untuk perintah `execv`. Argumen berisi:
       - "find": Perintah yang akan dijalankan.
       - "players": Nama direktori tempat file-file pemain berada.
       - "-type": Argumen untuk menentukan jenis kategori yang dicari.
       - "f": Argumen untuk mencari hanya file.
       - "!": Argumen untuk membalikkan hasil pencarian.
       - "-name": Argumen untuk mencari berdasarkan nama.
       - "*Cavaliers*.png": Pola nama file yang akan dicari.
       - "-delete": Argumen untuk menghapus hasil pencarian.
       - NULL: Akhir argumen.

     - `execv("/usr/bin/find", argv);`: Menggantikan proses saat ini dengan proses `find` menggunakan `execv`.

4. ```c
   wait(NULL);
   ```
   - Proses parent akan menunggu hingga proses child selesai.

6. #### `kategorikanPemain(char *namaFile, int *pg, int *sg, int *sf, int *pf, int *c)`
    - Fungsi ini memindahkan file pemain ke folder kategori yang sesuai (PG, SG, SF, PF, C) berdasarkan nama file.
    - Proses ini merupakan pengkondisian berdasarkan substring dari nama file dan menggunakan fork dan execv untuk memindahkan file.

### Penjelasan tiap bagian

```c
void kategorikanPemain(char *namaFile, int *pg, int *sg, int *sf, int *pf, int *c) {
```

- Fungsi `kategorikanPemain` menerima beberapa parameter:
  - `namaFile`: Nama file pemain yang akan dikategorikan.
  - `pg`, `sg`, `sf`, `pf`, `c`: Pointer ke variabel yang menyimpan jumlah pemain untuk setiap posisi/kategori.

```c
    if (strstr(namaFile, "PG") != NULL) {
        buatFolder("players/PG");
        pid_t child = fork();
        if (child == 0) {
            char command[100];
            sprintf(command, "mv players/%s players/PG/%s", namaFile, namaFile);
            char *argv[] = {"sh", "-c", command, NULL};
            execv("/bin/sh", argv);
        }
        wait(NULL);
        (*pg)++;
    } else if (strstr(namaFile, "SG") != NULL) {
        // ...
    } else if (strstr(namaFile, "SF") != NULL) {
        // ...
    } else if (strstr(namaFile, "PF") != NULL) {
        // ...
    } else if (strstr(namaFile, "C") != NULL) {
        // ...
    }
}
```

- Fungsi ini melakukan pengkategorian pemain berdasarkan posisi (PG, SG, SF, PF, C).
- Jika posisi Point Guard (PG), maka:
  - Membuat folder "players/PG" jika belum ada.
  - Memindahkan file pemain ke folder "players/PG".
  - Melacak jumlah pemain di posisi PG (`*pg`).
- Prosesnya sama untuk setiap jenis posisi.

7. #### `tambahFotoClutch()`
    - Fungsi ini memindahkan foto pemain PG Kyrie Irving ke folder `clutch`.
    - Proses ini juga dilakukan dengan melakukan fork, dan di dalam child process, menggunakan `execv` untuk menjalankan perintah `find` dan `mv` untuk move.

### Penjelasan tiap bagian

```c
void tambahFotoClutch() {
```

- Fungsi `tambahFotoClutch` digunakan untuk menambahkan foto pemain yang menang 7 game winning.

```c
    pid_t child = fork();
    if (child == 0) {
        char *argv[] = {"find", "players/PG", "-name", "*Cavaliers-PG-Kyrie-Irving.png", "-exec", "mv", "{}", "clutch/", ";", NULL};
        execv("/usr/bin/find", argv);
    }
    wait(NULL);
}
```

- Fungsi ini menggunakan fork untuk membuat proses child.
- Di dalam proses child (ketika `child` adalah 0), program menggunakan perintah `find` untuk mencari foto pemain Kyrie Irving".
- Setelah itu, perintah `-exec` digunakan untuk menjalankan perintah `mv` (move) pada hasil pencarian, memindahkan foto-foto tersebut ke direktori "clutch/".
- `";"` menandakan akhir perintah `-exec`.
- `NULL` menandakan akhir argumen.

8. #### `tambahFotoTheBlock()`
    - Fungsi ini memindahkan foto pemain SF LeBron James ke folder `clutch`.
    - Proses ini juga dilakukan dengan melakukan fork, dan di dalam child process, menggunakan `execv` untuk menjalankan perintah `find` dan `mv` untuk move.

### Penjelasan tiap bagian

```c
void tambahFotoTheBlock() {
```

- Fungsi `tambahFotoTheBlock` digunakan untuk menambahkan foto pemain LeBron James yang mendapat gelar "The Block".

```c
    pid_t child = fork();
    if (child == 0) {
        char *argv[] = {"find", "players/SF", "-name", "*Cavaliers-SF-LeBron-James.png", "-exec", "mv", "{}", "clutch/", ";", NULL};
        execv("/usr/bin/find", argv);
    }
    wait(NULL);
}
```

- Fungsi ini menggunakan fork untuk membuat proses child.
- Di dalam proses child (ketika `child` adalah 0), program menggunakan perintah `find` untuk mencari foto LeBron James.
- Setelah itu, perintah `-exec` akan digunakan untuk menjalankan perintah `mv` (move) pada setiap hasil pencarian, memindahkan foto-foto tersebut ke direktori "clutch/".
- `";"` menandakan akhir perintah `-exec`.
- `NULL` menandakan akhir argumen.

9. #### `buatFileFormasi()`
    - Fungsi ini membuat file `Formasi.txt` yang berisi jumlah pemain dalam setiap kategori (PG, SG, SF, PF, C).
    - File tersebut dibuat dalam mode menulis (`w`) dan menggunakan `fprintf` untuk menulis data ke dalam file.

### Penjelasan tiap bagian

```c
void buatFileFormasi() {
```

- Fungsi `buatFileFormasi` digunakan atau menulis informasi formasi pemain ke dalam file "Formasi.txt".

```c
    FILE *fp = fopen("Formasi.txt", "w");
    if (fp == NULL) {
        printf("Error creating file!\n");
        return;
    }
```

- Fungsi `fopen` digunakan untuk membuka file "Formasi.txt" dengan mode "w" (write), sehingga file akan dibuat jika belum ada atau ditimpa jika sudah ada. Hasil dari operasi ini akan disimpan dalam pointer `fp`.
- Jika `fopen` mengembalikan nilai NULL, maka terjadi error dalam pembuatan atau pembukaan file.

```c
    fprintf(fp, "PG: %d\nSG: %d\nSF: %d\nPF: %d\nC: %d\n", pg, sg, sf, pf, c);
    fclose(fp);
}
```

- `fprintf` digunakan untuk menulis informasi formasi ke dalam file yang telah dibuka. Format informasi adalah "NamaPosisi: JumlahPemain".
- `fclose(fp)` digunakan untuk menutup file setelah selesai ditulis.

10. #### `main()`
    - Fungsi `main` adalah fungsi utama dari program pengelolaan data pemain basker.
    - Di dalam `main`, proses-proses diurutkan untuk inisialisasi dan dioperasikan, termasuk membuat folder, mengunduh data pemain, mengekstrak data, menghapus file zip, mengkategorikan pemain, dan lain sebagainya.

### Penjelasan tiap bagian

```c
int main() {
```

- Ini adalah fungsi utama program.

```c
    buatFolder("players");
    unduhDatabasePemain();
    ekstrakDatabase();
    hapusDatabaseZip();
    hapusPemainSelainCavs();
```

- Langkah pertama menyiapkan folder "players", mengunduh database pemain, mengekstrak databasenya, menghapus file zip setelah diekstrak, dan menghapus pemain yang bukan dari tim Cavaliers.

```c
    FILE *fileList = popen("ls players", "r");
    char namaFile[100];

    while (fgets(namaFile, sizeof(namaFile), fileList)) {
        namaFile[strlen(namaFile)-1] = '\0'; 
        kategorikanPemain(namaFile, &pg, &sg, &sf, &pf, &c);
    }

    pclose(fileList);
```

- Program membuka direktori "players" dan membaca nama file yang ada di dalamnya menggunakan perintah `ls players`.
- Kemudian, setiap nama file dibaca satu per satu dan diteruskan ke fungsi `kategorikanPemain` untuk dikategorikan sesuai posisi pemainnya.

```c
    buatFolder("clutch");
    tambahFotoClutch();
    tambahFotoTheBlock();
```

- Program membuat folder "clutch" dan menambahkan foto-foto pemain yang menang 7 game winning serta foto LeBron James yang mendapat gelar "The Block" ke dalam folder tersebut.

```c
    buatFileFormasi();
```

- Akhirnya, program membuat file "Formasi.txt" yang berisi informasi jumlah pemain untuk setiap posisi.

```c
    return 0;
}
```

- Fungsi `main` mengembalikan nilai 0 bahwa program berjalan dengan lancar.

![no 2 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/docum%20soal_2.png)
![no 2 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/docum%20formasi.png)


## PENJELASAN JAWABAN

### SOAL NO 3 (lukisan.c)

1. **Deklarasi Library**:
   ```c
   #include <sys/types.h>
   #include <sys/stat.h>
   #include <stdio.h>
   #include <stdlib.h>
   #include <fcntl.h>
   #include <errno.h>
   #include <unistd.h>
   #include <syslog.h>
   #include <string.h>
   #include <time.h>
   #include <sys/wait.h>
   #include <signal.h>
   ```
   - Kode diawali dengan beberapa pernyataan `#include` yang menyertakan berbagai file header (header files) yang diperlukan untuk program ini. Header files ini memungkinkan penggunaan fungsi-fungsi dari sistem operasi dan pustaka-pustaka standar C.

2. **fungsi buatFolder**:
   ```c
   void buatFolder(char* timestamp) {
       // ...
   }
   ```
   - Fungsi ini membuat sebuah folder dengan nama yang diberikan dalam parameter `timestamp`.
   - Membuat proses baru dengan menggunakan `fork()`, kemudian menjalankan perintah `mkdir` dengan menggunakan `execlp()`.

3. **fungsi downloadImages**:
   ```c
   void downloadImages(char* timestamp) {
       // ...
   }
   ```
   - Fungsi ini melakukan pengunduhan gambar dari sumber yang disediakan oleh unsplash.
   - Menggunakan loop untuk melakukan pengunduhan sebanyak 15 kali.
   - Dalam setiap iterasi, membuat proses baru dengan menggunakan `fork()`, dan menggunakan `execv()` untuk menjalankan perintah `wget`.

4. **fungsi zipFolder**:
   ```c
   void zipFolder(char* timestamp) {
       // ...
   }
   ```
   - Fungsi ini melakukan kompresi folder dengan menggunakan perintah `zip`.
   - Memanggil `wait()` untuk menunggu proses sebelumnya selesai sebelum melanjutkan.

5. **fungsi killScript**:
   ```c
   void killScript(char* mode) {
       // ...
   }
   ```
   - Fungsi ini membuat sebuah script bash untuk membunuh proses dengan nama "lukisan" atau untuk membunuh proses induk "lukisan" beserta semua anak-anaknya.
   - Membuat file `kill.sh`, menulis script bash di dalamnya, dan memberikan izin eksekusi dengan `chmod`.
  
6. **fungsi main**:
   ```c
   int main(int argc, char** argv) {
       // ...
   }
   ```
   - Fungsi `main` adalah titik masuk utama dari program.
   - Memeriksa apakah argumen yang diberikan sesuai dengan yang diharapkan (yaitu `-a` atau `-b`).
   - Memulai daemon dengan menggunakan `setsid()` untuk memisahkan proses dari terminal.
   - Menutup file descriptor standar (stdin, stdout, stderr).
   - Memulai loop tak terbatas yang akan menjalankan fungsi-fungsi untuk membuat folder, mengunduh gambar, dan mengompresi folder dalam interval 30 detik.
   - Terakhir, memanggil `killScript()` untuk membuat dan menjalankan script bash yang dapat membunuh proses "lukisan".


## DOCUMENTASI

![no 3 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/docum%20soal_3.png)
![no 3 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/docum%20soal3%20lagi.png)


## PENJELASAN JAWABAN

### SOAL NO 4 (antivirus.c)

1. **Deklarasi Library**:
   ```c
   #include <stdio.h>
   #include <stdlib.h>
   #include <unistd.h>
   #include <sys/wait.h>
   #include <sys/types.h>
   #include <sys/stat.h>
   #include <fcntl.h>
   #include <string.h>
   #include <time.h>
   #include <dirent.h>
   #include <stdbool.h>
   ```
   - Kode diawali dengan beberapa pernyataan `#include` yang menyertakan berbagai file header (header files) yang diperlukan untuk program ini. Header files ini memungkinkan penggunaan fungsi-fungsi dari sistem operasi dan pustaka-pustaka standar C.

2. **fungsi buatFolder**:
   ```c
   void buatFolder(const char *namaFolder) {
       // ...
   }
   ```
   - Fungsi ini membuat folder jika folder tersebut belum ada. Menggunakan `stat` untuk memeriksa apakah folder sudah ada atau belum, dan `mkdir` untuk membuatnya jika belum ada.

3. **fungsi unduhBerkas**:
   ```c
   void unduhBerkas() {
       // ...
   }
   ```
   - Fungsi ini membuat folder "sisop_terinfeksi", pindah ke dalamnya, dan menggunakan `execvp` untuk menjalankan perintah `curl` yang akan mengunduh berkas dari URL yang diberikan.

4. **fungsi simpanBerkas**:
   ```c
   void simpanBerkas() {
       // ...
   }
   ```
   - Fungsi ini memeriksa apakah berkas "sisop_terinfeksi/ekstensi.csv" sudah ada atau belum. Jika belum, maka akan membuat proses anak untuk menjalankan fungsi `unduhBerkas`.

5. **fungsi periksaBerkas**:
   ```c
   void periksaBerkas() {
       // ...
   }
   ```
   - Fungsi ini membuka berkas "sisop_terinfeksi/terdekripsi.csv" untuk dibaca. Kemudian, membaca setiap baris dari berkas tersebut dan memeriksa apakah ada berkas di dalam folder "sisop_terinfeksi" dengan ekstensi yang cocok dengan berkas yang terdekripsi. Jika ada, maka akan memanggil fungsi `buatLog` untuk mencatat aktivitas ini.

6. **fungsi buatLog**:
   ```c
   void buatLog(const char *baris, const char *aksi) {
       // ...
   }
   ```
   - Fungsi ini digunakan untuk membuat catatan log ke dalam berkas "virus.log". Log akan mencatat waktu, nama pengguna, nama berkas, dan tindakan yang dilakukan.

7. **fungsi dekripsiBerkas**:
   ```c
   void dekripsiBerkas() {
       // ...
   }
   ```
   - Fungsi ini membuka berkas "sisop_terinfeksi/ekstensi.csv" untuk dibaca dan "sisop_terinfeksi/terdekripsi.csv" untuk ditulis. Kemudian, membaca baris-baris dari berkas terenkripsi, mendekripsi isi berkas, dan menyimpannya di berkas terdekripsi.

8. **fungsi main**:
   ```c
   int main(int argc, char const *argv[]) {
       // ...
   }
   ```
   - Fungsi `main` adalah titik masuk utama dari program.
   - Memeriksa apakah jumlah argumen dari baris perintah sesuai dengan yang diharapkan. Jika tidak sesuai, maka akan mencetak pesan kesalahan dan keluar dari program.
   - Mengambil mode dari argumen baris perintah dan menggunakan `switch` untuk menentukan tindakan selanjutnya berdasarkan mode yang diberikan.
   - Pada akhirnya, program akan mengembalikan nilai 0 untuk menandakan bahwa program berakhir dengan sukses.


## DOCUMENTASI

![no 4 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/docum%20soal%204.png)

## KENDALA PADA NO 4
tidak terdeteksi dari virus yang dioutputkan pada terminal
